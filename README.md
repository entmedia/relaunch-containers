# relaunch-conatiners

This repo will configure the 🚀Relaunch enviornment.

## Containers

This docker compose file build 3 containers

1. Apollo graphlql Server
1. Sapper application server
1. StorybookJS server

## Development

To use these continers for development you need to do the following:

1. Edit your `/etc/hosts` file to include the entries:
    - `127.0.0.1   graphql`
    - `127.0.0.1   sapper`
1. Clone the [https://bitbucket.org/entmedia/api-nodejs/src/dev/](https://bitbucket.org/entmedia/api-nodejs/src/dev/) repo into a peer directory
1. Clone the [https://bitbucket.org/entmedia/entrepreneur-front-end-sapper/src/develop/](https://bitbucket.org/entmedia/entrepreneur-front-end-sapper/src/develop/) repo into a peer directory
1. Have [Docker Desktop](https://www.docker.com/products/docker-desktop) or similar solution installed on your machine
1. You may need to have your local machine VPN into the the network to be able to access the Riak data store 
1. Run `docker-compose up` which will build and run the environment

## Endpoints

- The graphql endpoint is at `http://graphql:4000`
- The webapp endpoint is at `http://sapper:3000`
- The storybook endpoint is at `http://localhost:6006`

## ToDo

1. Create a production build of the environment
1. Move configuration params into environment variables to be passed into the container
